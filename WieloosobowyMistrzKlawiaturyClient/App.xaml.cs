﻿using System.Windows;

namespace WieloosobowyMistrzKlawiaturyClient
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {
        void Application_Startup(object sender, StartupEventArgs e)
        {
            // Application is running
            // Process command line args
            for (int i = 0; i != e.Args.Length; ++i)
            {
                // handle args
            }

            mainWindow = new MainWindow();
            mainWindow.Show();
        }

        private void Application_Exit(object sender, ExitEventArgs e)
        {
            mainWindow.SendDisconnectMessage();
        }

        MainWindow mainWindow;
    }
}
