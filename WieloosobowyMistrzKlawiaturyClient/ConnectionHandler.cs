﻿using System;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using WieloosobowyMistrzKlawiaturyLib.Game;
using WieloosobowyMistrzKlawiaturyLib.Utils;
using static WieloosobowyMistrzKlawiaturyLib.Utils.Logging;

namespace WieloosobowyMistrzKlawiaturyClient
{
    /// <summary>
    /// Class for communicating with the server
    /// </summary>
    public class ConnectionHandler
    {
        public ConnectionHandler(string serverAddress)
        {
            this.serverAddress = serverAddress;
        }

        public bool HandleInitialCommunication()
        {
            if (!EstablishCommunication())
            {
                return false;
            }

            Send("HASH;" + new FilesUtils("data/texts").GetHashOfDataDir());
            string response = Receive();
            if (response != "HASH SUCCESSFUL")
            {
                LogError("HASH failed, response: " + response);
                return false;
            }

            return true;
        }

        public bool PresentYourself()
        {
            Send("IM;" + PlayerName);
            string response = Receive();
            if (response != "IM SUCCESSFUL")
            {
                LogError("IM failed, response: " + response);
                return false;
            }

            return true;
        }

        public JoinableGame[] GetListOfCurrentlyHostedGames()
        {
            Send("LIST");
            string response = Receive();
            if (response.Length == 1)
            {
                return null;
            }
            return response.Split('\n')
                .Skip(1)
                .SkipLast(1)
                .Where((string gameString) => gameString.Split(';').Length >= 3)
                .Select((string gameString) =>
                {
                    string[] parts = gameString.Split(';');
                    string gameName = parts[0];
                    FileCategory gameCategory = Enum.Parse<FileCategory>(parts[1]);
                    string hostName = parts[2];
                    return new JoinableGame(gameName, hostName, gameCategory);
                }).ToArray();
        }

        public Player[] GetListOfPlayersInTheGame()
        {
            Send("PLAYERS");
            string response = Receive();
            if (response == "PLAYERS NOT INGAME")
            {
                return null;
            }
            return response.Split(';')
                .Select((string playerName) => new Player(playerName)).ToArray();
        }

        public bool CreateNewGame(string gameName, FileCategory gameCategory)
        {
            Send("CREATE;" + gameName + ";" + gameCategory);
            string response = Receive();
            if (response != "CREATE SUCCESSFUL")
            {
                LogError("CREATE failed, response: " + response);
                return false;
            }

            return true;
        }

        public bool JoinGame(string gameName)
        {
            Send("JOIN;" + gameName);
            string response = Receive();
            if (response != "JOIN SUCCESSFUL")
            {
                LogError("JOIN failed, response: " + response);
                return false;
            }

            return true;
        }

        /// <summary>
        /// Indicate that you are ready to play
        /// </summary>
        /// <returns>Whether the request was properly handled</returns>
        public bool StartGame()
        {
            Send("START");
            string response = Receive();
            return response == "START SUCCESSFUL";
        }

        public void WaitForFullGameStart()
        {
            while (true)
            {
                if (Receive() == "GAME STARTING")
                {
                    return;
                }
            }
        }

        public bool EndGame()
        {
            Send("END");
            string response = Receive();
            return response == "END SUCCESSFUL";
        }

        public void SendDisconnectMessage()
        {
            Send("DC");
        }

        private bool EstablishCommunication()
        {
            string ipPart = serverAddress.Substring(0, serverAddress.IndexOf(':'));
            int portPart = int.Parse(serverAddress.Substring(ipPart.Length + 1));

            if (ipPart == "localhost")
            {
                ipPart = "127.0.0.1";
            }

            try
            {
                IPHostEntry hostEntry = Dns.GetHostEntry(ipPart);
                foreach (IPAddress address in hostEntry.AddressList)
                {
                    IPEndPoint ipe = new IPEndPoint(address, portPart);
                    Socket tempSocket = new Socket(ipe.AddressFamily, SocketType.Stream, ProtocolType.Tcp);

                    tempSocket.Connect(ipe);

                    if (tempSocket.Connected)
                    {
                        serverSocket = tempSocket;
                        return true;
                    }
                    else
                    {
                        continue;
                    }
                }
            }
            catch (SocketException ex)
            {
                LogError(ex.ToString());
                return false;
            }
            return false;
        }

        private void Send(string text)
        {
            serverSocket.Send(Encoding.ASCII.GetBytes(text));
        }

        private string Receive()
        {
            byte[] buffer = new byte[1024];
            int receivedBytes = serverSocket.Receive(buffer);
            return Encoding.ASCII.GetString(buffer, 0, receivedBytes);
        }

        private string serverAddress;
        public string PlayerName { get; set; }
        private Socket serverSocket;
    }

    public class JoinableGame
    {
        public JoinableGame(string gameName, string hostName, FileCategory category)
        {
            GameName = gameName;
            HostName = hostName;
            Category = category;
        }

        public string GameName { get; }
        public string HostName { get; }
        public FileCategory Category { get; }
    }
}
