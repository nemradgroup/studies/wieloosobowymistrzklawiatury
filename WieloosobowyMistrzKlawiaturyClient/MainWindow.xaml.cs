﻿using System;
using System.Text.RegularExpressions;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Threading;
using WieloosobowyMistrzKlawiaturyLib.Game;

namespace WieloosobowyMistrzKlawiaturyClient
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
            ServerAddressInputBox.Focus();

            lobbyRefreshTimer = new DispatcherTimer() { Interval = new TimeSpan(0, 0, 1) }; // 1 sec
            lobbyRefreshTimer.Tick += new EventHandler((object sender, EventArgs e) => FillLobbyScreen());

            gameScreenRefreshTimer = new DispatcherTimer() { Interval = new TimeSpan(0, 0, 0, 0, 500) }; // 0.5 sec
            gameScreenRefreshTimer.Tick += new EventHandler((object sender, EventArgs e) => FillGameScreen());
        }

        public void SendDisconnectMessage()
        {
            try
            {
                connectionHandler?.SendDisconnectMessage();
            }
            catch (System.Net.Sockets.SocketException)
            {
                ConnectionToServerClosedDialog.Visibility = Visibility.Visible;
            }
        }

        private void FillJoinHostedGameButtons()
        {
            int i = 0;
            for (; i < JoinHostedGameStack.Children.Count; ++i)
            {
                if (currentlyHostedGames == null || currentOffsetOnHostedGamesList + i >= currentlyHostedGames.Length)
                {
                    break;
                }

                JoinableGame game = currentlyHostedGames[currentOffsetOnHostedGamesList + i];
                (JoinHostedGameStack.Children[i] as Button).Content = game.GameName + " (Host: " + game.HostName + ", Category: " + game.Category + ")";
                (JoinHostedGameStack.Children[i] as Button).IsEnabled = true;
            }
            for (; i < JoinHostedGameStack.Children.Count; ++i)
            {
                (JoinHostedGameStack.Children[i] as Button).Content = "";
                (JoinHostedGameStack.Children[i] as Button).IsEnabled = false;
            }
        }

        private void CreateCategoriesRadioButtons()
        {
            CategoriesStack.Children.Clear();
            foreach (string fileCategory in Enum.GetNames(typeof(FileCategory)))
            {
                RadioButton button = new RadioButton() { GroupName = "CategoriesRadioButtons", Content = fileCategory, Margin = new Thickness(5.0) };
                CategoriesStack.Children.Add(button);
            }
            (CategoriesStack.Children[0] as RadioButton).IsChecked = true;
        }

        private void FillLobbyScreen()
        {
            LobbyHeader.Text = nameOfCreatedGame;

            if (LobbyPlayersStack.Children.Count > 1)
            {
                LobbyPlayersStack.Children.RemoveRange(1, LobbyPlayersStack.Children.Count - 1);
            }
            Player[] players = connectionHandler.GetListOfPlayersInTheGame();
            if (players != null)
            {
                foreach (Player player in players)
                {
                    TextBlock textBlock = new TextBlock() { Text = player.Name, Margin = new Thickness(5), FontFamily = new System.Windows.Media.FontFamily("Cambria") };
                    if (player.Name == connectionHandler.PlayerName)
                    {
                        textBlock.FontWeight = FontWeights.Bold;
                    }
                    LobbyPlayersStack.Children.Add(textBlock);
                }
            }

            StartGame.IsEnabled = players.Length - 1 == 2;
        }

        private void FillGameScreen()
        {
            // TODO
        }

        // 'slots'

        private void EnterServerAddressButton_Click(object sender, RoutedEventArgs e)
        {
            ServerAddressInputDialog.Visibility = Visibility.Collapsed;

            string serverAddress = ServerAddressInputBox.Text;

            Regex addressRegex = new Regex("^(?:[a-zA-Z0-9]+|[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}):[0-9]{1,5}$");
            if (addressRegex.IsMatch(serverAddress))
            {
                connectionHandler = new ConnectionHandler(serverAddress);
                if (connectionHandler.HandleInitialCommunication())
                {
                    ConnectedToServerEnterNameDialog.Visibility = Visibility.Visible;
                    EnterNameInputBox.Focus();
                }
                else
                {
                    CouldntConnectTryAgainDialog.Visibility = Visibility.Visible;
                }
            }
            else
            {
                ServerAddressInvalidDialog.Visibility = Visibility.Visible;
            }
        }

        private void ServerAddressInvalidAcknowledged_Click(object sender, RoutedEventArgs e)
        {
            ServerAddressInvalidDialog.Visibility = Visibility.Collapsed;
            ServerAddressInputDialog.Visibility = Visibility.Visible;
            ServerAddressInputBox.Focus();
        }

        private void EnterNameOk_Click(object sender, RoutedEventArgs e)
        {
            ConnectedToServerEnterNameDialog.Visibility = Visibility.Collapsed;

            string name = EnterNameInputBox.Text;
            if (name.Length > 0 && !name.Contains(';'))
            {
                connectionHandler.PlayerName = name;
                try
                {
                    if (connectionHandler.PresentYourself())
                    {
                        MainMenu.Visibility = Visibility.Visible;
                    }
                    else
                    {
                        InvalidEnteredNameDialog.Visibility = Visibility.Visible;
                    }
                }
                catch (System.Net.Sockets.SocketException)
                {
                    ConnectionToServerClosedDialog.Visibility = Visibility.Visible;
                }
            }
            else
            {
                InvalidEnteredNameDialog.Visibility = Visibility.Visible;
            }
        }

        private void InvalidNameOk_Click(object sender, RoutedEventArgs e)
        {
            InvalidEnteredNameDialog.Visibility = Visibility.Collapsed;
            ConnectedToServerEnterNameDialog.Visibility = Visibility.Visible;
            EnterNameInputBox.Focus();
        }

        private void InvalidHashOk_Click(object sender, RoutedEventArgs e)
        {
            InvalidHashDialog.Visibility = Visibility.Collapsed;
            Close();
        }

        private void TryConnectingAgainButton_Click(object sender, RoutedEventArgs e)
        {
            CouldntConnectTryAgainDialog.Visibility = Visibility.Collapsed;
            if (connectionHandler.HandleInitialCommunication())
            {
                ConnectedToServerEnterNameDialog.Visibility = Visibility.Visible;
                EnterNameInputBox.Focus();
            }
            else
            {
                CouldntConnectTryAgainDialog.Visibility = Visibility.Visible;
            }
        }

        private void ReenterServerAddressButton_Click(object sender, RoutedEventArgs e)
        {
            CouldntConnectTryAgainDialog.Visibility = Visibility.Collapsed;
            ServerAddressInputDialog.Visibility = Visibility.Visible;
            ServerAddressInputBox.Focus();
        }

        private void ListGamesToJoinButton_Click(object sender, RoutedEventArgs e)
        {
            MainMenu.Visibility = Visibility.Collapsed;
            ChooseGameToJoinPanel.Visibility = Visibility.Visible;

            currentOffsetOnHostedGamesList = 0;
            try
            {
                currentlyHostedGames = connectionHandler.GetListOfCurrentlyHostedGames();
            }
            catch (System.Net.Sockets.SocketException)
            {
                ConnectionToServerClosedDialog.Visibility = Visibility.Visible;
            }

            GoUpInHostedGamesList.IsEnabled = false;
            if (currentlyHostedGames != null)
            {
                GoDownInHostedGamesList.IsEnabled = currentlyHostedGames.Length > JoinHostedGameStack.Children.Count;
            }
            else
            {
                GoDownInHostedGamesList.IsEnabled = false;
            }

            FillJoinHostedGameButtons();
        }

        private void HostGameButton_Click(object sender, RoutedEventArgs e)
        {
            MainMenu.Visibility = Visibility.Collapsed;

            nameOfCreatedGame = null;
            categoryOfCreatedGame = FileCategory.Literature;
            EnterNameForHostedGameInputBox.Text = "";
            CreateCategoriesRadioButtons();

            ChooseNameForHostedGameDialog.Visibility = Visibility.Visible;
            EnterNameForHostedGameInputBox.Focus();
        }

        private void JoinHostedGame_Click(object sender, RoutedEventArgs e)
        {
            int i = 0;
            foreach (object button in JoinHostedGameStack.Children)
            {
                if (ReferenceEquals(button, sender))
                {
                    JoinableGame game = currentlyHostedGames[currentOffsetOnHostedGamesList + i];
                    bool joined = connectionHandler.JoinGame(game.GameName);
                    if (joined)
                    {
                        nameOfCreatedGame = game.GameName;
                        lobbyRefreshTimer.Start();
                        FillLobbyScreen();
                        ChooseGameToJoinPanel.Visibility = Visibility.Collapsed;
                        LobbyGrid.Visibility = Visibility.Visible;
                    }
                    else
                    {
                        // TODO error dialog
                    }
                }
                ++i;
            }
        }

        private void GoUpInHostedGamesList_Click(object sender, RoutedEventArgs e)
        {
            --currentOffsetOnHostedGamesList;
            FillJoinHostedGameButtons();
        }

        private void GoDownInHostedGamesList_Click(object sender, RoutedEventArgs e)
        {
            ++currentOffsetOnHostedGamesList;
            GoUpInHostedGamesList.IsEnabled = currentOffsetOnHostedGamesList > 0;
            GoDownInHostedGamesList.IsEnabled = currentlyHostedGames.Length > currentOffsetOnHostedGamesList + JoinHostedGameStack.Children.Count;
            FillJoinHostedGameButtons();
        }

        private void GoBackToMainMenu_Click(object sender, RoutedEventArgs e)
        {
            ChooseGameToJoinPanel.Visibility = Visibility.Collapsed;
            ChooseNameForHostedGameDialog.Visibility = Visibility.Collapsed;
            InvalidNameForHostedGameDialog.Visibility = Visibility.Collapsed;
            MainMenu.Visibility = Visibility.Visible;
        }

        private void EnterNameForHostedGameButton_Click(object sender, RoutedEventArgs e)
        {
            ChooseNameForHostedGameDialog.Visibility = Visibility.Collapsed;

            string name = EnterNameForHostedGameInputBox.Text;
            if (name.Length > 0 && !name.Contains(';'))
            {
                nameOfCreatedGame = name;
                ChooseCategoryForHostedGameDialog.Visibility = Visibility.Visible;
            }
            else
            {
                InvalidNameForHostedGameDialog.Visibility = Visibility.Visible;
            }
        }

        private void OkEnterHostedGameNameAgainButton_Click(object sender, RoutedEventArgs e)
        {
            InvalidNameForHostedGameDialog.Visibility = Visibility.Collapsed;
            ChooseNameForHostedGameDialog.Visibility = Visibility.Visible;
            EnterNameForHostedGameInputBox.Focus();
        }

        private void ChooseCategoryForHostedGameButton_Click(object sender, RoutedEventArgs e)
        {
            ChooseCategoryForHostedGameDialog.Visibility = Visibility.Collapsed;

            foreach (RadioButton button in CategoriesStack.Children)
            {
                try
                {
                    if ((bool)button.IsChecked)
                    {
                        string category = button.Content.ToString();
                        categoryOfCreatedGame = Enum.Parse<FileCategory>(category);
                        break;
                    }
                }
                catch (Exception) { }
            }

            if (connectionHandler.CreateNewGame(nameOfCreatedGame, categoryOfCreatedGame))
            {
                lobbyRefreshTimer.Start();
                FillLobbyScreen();
                LobbyGrid.Visibility = Visibility.Visible;
            }
            else
            {
                // TODO error dialog
            }
        }

        private void GoBackToChoosingNameForHostedGameButton_Click(object sender, RoutedEventArgs e)
        {
            ChooseCategoryForHostedGameDialog.Visibility = Visibility.Collapsed;
            ChooseNameForHostedGameDialog.Visibility = Visibility.Visible;
        }

        private void StartGame_Click(object sender, RoutedEventArgs e)
        {
            lobbyRefreshTimer.Stop();
            StartGame.IsEnabled = false;
            QuitLobby.IsEnabled = false;
            connectionHandler.StartGame();

            connectionHandler.WaitForFullGameStart(); // block until game starts // TODO don't freeze UI, run in parallel thread

            gameScreenRefreshTimer.Start();
            GameGrid.Visibility = Visibility.Visible;
            // TODO
        }

        private void ExitLobby_Click(object sender, RoutedEventArgs e)
        {
            LobbyGrid.Visibility = Visibility.Collapsed;
            MainMenu.Visibility = Visibility.Visible;
            connectionHandler.EndGame();
        }

        private void CloseApplicationButton_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }

        private ConnectionHandler connectionHandler;

        private int currentOffsetOnHostedGamesList = 0;
        private JoinableGame[] currentlyHostedGames;

        private string nameOfCreatedGame;
        private FileCategory categoryOfCreatedGame = FileCategory.Literature;

        private DispatcherTimer lobbyRefreshTimer;
        private DispatcherTimer gameScreenRefreshTimer;
    }
}
