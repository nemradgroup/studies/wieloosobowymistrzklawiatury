﻿namespace WieloosobowyMistrzKlawiaturyLib.Game
{
    public enum FileCategory
    {
        Literature,
        Code,
        Latex
    }
}
