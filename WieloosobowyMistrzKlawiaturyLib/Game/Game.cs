﻿namespace WieloosobowyMistrzKlawiaturyLib.Game
{
    /// <summary>
    /// A single game between two players
    /// </summary>
    public class Game
    {
        public Game(FileCategory category, string name)
        {
            Category = category;
            Name = name;
        }

        public bool CanPlayerJoin() => currentPlayers < maxPlayers;

        public void AddPlayer(Player player) => Players[currentPlayers++] = player;

        static private readonly int maxPlayers = 2;
        private int currentPlayers = 0;
        public Player[] Players { get; } = new Player[maxPlayers];
        public FileCategory Category { get; }
        public string Name { get; }
    }
}
