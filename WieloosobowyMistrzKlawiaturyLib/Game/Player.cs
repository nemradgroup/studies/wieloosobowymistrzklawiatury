﻿namespace WieloosobowyMistrzKlawiaturyLib.Game
{
    /// <summary>
    /// Represents a player in a game, their current status, points
    /// </summary>
    public class Player
    {
        public Player(string name)
        {
            Name = name;
        }

        public string Name { get; }
        public int Score { get; } = 0;
    }
}
