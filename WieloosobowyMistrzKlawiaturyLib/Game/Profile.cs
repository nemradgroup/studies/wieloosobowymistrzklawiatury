﻿namespace WieloosobowyMistrzKlawiaturyLib.Game
{
    /// <summary>
    /// Player's profile, including name, stats
    /// </summary>
    public class Profile
    {
        public string Name { get; set; }
    }
}
