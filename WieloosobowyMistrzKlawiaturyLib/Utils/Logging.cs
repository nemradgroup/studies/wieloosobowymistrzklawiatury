﻿using System;

namespace WieloosobowyMistrzKlawiaturyLib.Utils
{
    public static class Logging
    {
        public static void LogInfo(string info)
        {
            Console.WriteLine(info);
        }

        public static void LogError(string error)
        {
            Console.Error.WriteLine(error);
        }
    }
}
