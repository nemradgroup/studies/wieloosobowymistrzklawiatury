﻿using System.Net;
using System.Net.Sockets;

namespace WieloosobowyMistrzKlawiaturyLib.Utils
{
    public static class NetworkUtils
    {
        public struct IPPort
        {
            public readonly string ip;
            public readonly int port;

            public IPPort(string ip, int port)
            {
                this.ip = ip;
                this.port = port;
            }
        }

        public static IPPort GetIPv4AndPort(Socket socket)
        {
            IPEndPoint remoteEndpoint = socket.RemoteEndPoint as IPEndPoint;
            return new IPPort(remoteEndpoint.Address.MapToIPv4().ToString(), remoteEndpoint.Port);
        }
    }
}
