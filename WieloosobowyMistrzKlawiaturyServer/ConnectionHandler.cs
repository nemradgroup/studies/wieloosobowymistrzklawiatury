﻿using System;
using System.Collections.Generic;
using System.Net.Sockets;
using System.Text;
using WieloosobowyMistrzKlawiaturyLib.Game;
using static WieloosobowyMistrzKlawiaturyLib.Utils.NetworkUtils;
using static WieloosobowyMistrzKlawiaturyLib.Utils.Logging;

namespace WieloosobowyMistrzKlawiaturyServer
{
    /// <summary>
    /// Handles the connection with the client
    /// </summary>
    class ConnectionHandler
    {
        public enum ClientStatus
        {
            Undefined,
            OutsideGame,
            InGame
        }

        public ConnectionHandler(List<Game> games, string hashOfServerFiles, Socket clientSocket, Action<Game, Socket> playerReadyNotifier)
        {
            this.games = games;
            this.serverHash = hashOfServerFiles;
            this.clientSocket = clientSocket;
            this.playerReadyNotifier = playerReadyNotifier;
        }

        public bool HandleOutsideGame()
        {
            while (true)
            {
                try
                {
                    string message = Receive();
                    if (message == "LIST")
                    {
                        /// List hosted games
                        /// 
                        /// Request:
                        /// LIST
                        /// 
                        /// Response:
                        /// GAME1_NAME PLAYER1_NAME\n
                        /// GAME2_NAME PLAYER1_NAME PLAYER2_NAME\n
                        /// 

                        string response = "\n";
                        foreach (Game game in games)
                        {
                            if (game.CanPlayerJoin())
                            {
                                response += game.Name + ";" + game.Category;
                                foreach (Player player in game.Players)
                                {
                                    if (player == null)
                                    {
                                        break;
                                    }
                                    response += ";" + player.Name;
                                }
                                response += "\n";
                            }
                        }
                        Send(response);
                    }
                    else if (message == "PLAYERS")
                    {
                        /// List players in the current game
                        /// 
                        /// Request:
                        /// PLAYERS
                        /// 
                        /// Response:
                        /// PLAYERS NOT INGAME
                        /// or
                        /// PLAYER_NAME1;PLAYER_NAME2
                        /// 

                        if (clientGame == null)
                        {
                            LogError("Client tried listing players in their game but wasn't ingame");
                            Send("PLAYERS NOT INGAME");
                            continue;
                        }

                        string response = "";
                        foreach (Player player in clientGame.Players)
                        {
                            if (player == null)
                            {
                                break;
                            }
                            response += player.Name + ";";
                        }
                        response.Remove(response.Length - 1);

                        Send(response);
                    }
                    else if (message.StartsWith("CREATE;") && message.Length > "CREATE;".Length)
                    {
                        /// Host a new game:
                        /// 
                        /// Request:
                        /// CREATE;GAME_NAME
                        /// 
                        /// Response:
                        /// CREATE ILLFORMED
                        /// or
                        /// CREATE NOT READY
                        /// or
                        /// CREATE SUCCESSFUL
                        /// 

                        string args = message.Substring("CREATE;".Length);
                        int catIndex = args.IndexOf(';');
                        string gameName = args.Substring(0, catIndex);
                        string catName = args.Substring(catIndex + 1);
                        if (catName.Contains(';'))
                        {
                            LogError("Encountered bad CREATE request: " + message);
                            Send("CREATE ILLFORMED");
                            continue;
                        }

                        if (!IsReadyForGame())
                        {
                            LogError("Client tried creating a game but wasn't ready");
                            Send("CREATE NOT READY");
                            continue;
                        }

                        clientGame = new Game(Enum.Parse<FileCategory>(catName), gameName);
                        clientGame.AddPlayer(new Player(clientName));
                        games.Add(clientGame);

                        LogInfo("Client: " + clientName + " created a game: " + gameName);
                        Send("CREATE SUCCESSFUL");
                    }
                    else if (message.StartsWith("JOIN;") && message.Length > "JOIN;".Length)
                    {
                        /// Join a game
                        /// 
                        /// Request:
                        /// JOIN;GAME_NAME
                        /// 
                        /// Response:
                        /// JOIN ILLFORMED
                        /// or
                        /// JOIN NOT READY
                        /// or
                        /// JOIN SUCCESSFUL
                        /// or
                        /// JOIN NOT FOUND
                        /// 

                        string gameName = message.Substring("JOIN;".Length);
                        if (gameName.Contains(';')) // TODO this if (and next ones) is wrong, fix it
                        {
                            LogError("Encountered bad JOIN request: " + message);
                            Send("JOIN ILLFORMED");
                            continue;
                        }

                        if (!IsReadyForGame())
                        {
                            LogError("Client tried joining a game but wasn't ready");
                            Send("JOIN NOT READY");
                            continue;
                        }

                        bool joined = false;
                        foreach (Game game in games)
                        {
                            if (game.CanPlayerJoin() && game.Name == gameName)
                            {
                                game.AddPlayer(new Player(clientName));
                                clientGame = game;
                                LogInfo("Client: " + clientName + " joined a game: " + gameName);
                                Send("JOIN SUCCESSFUL");
                                joined = true;
                                break;
                            }
                        }

                        if (!joined)
                        {
                            LogError("Couldn't find the game specified in JOIN: " + gameName);
                            Send("JOIN NOT FOUND");
                        }
                    }
                    else if (message == "START")
                    {
                        /// Start the game
                        /// 
                        /// Request:
                        /// START
                        /// 
                        /// Response:
                        /// START NOT INGAME
                        /// or
                        /// START SUCCESSFUL
                        /// 

                        if (clientGame == null)
                        {
                            LogError("START requested for non-existing game");
                            Send("START NOT INGAME");
                            continue;
                        }

                        Send("START SUCCESSFUL");

                        playerReadyNotifier(clientGame, clientSocket);

                        return true;
                    }
                    else if (message == "END")
                    {
                        /// End the game
                        /// 
                        /// Request:
                        /// END
                        /// 
                        /// Response:
                        /// END NOT INGAME
                        /// or
                        /// END SUCCESSFUL
                        /// 

                        if (clientGame == null)
                        {
                            LogError("END requested for non-existing game");
                            Send("END NOT INGAME");
                            continue;
                        }

                        EndGame();
                        Send("END SUCCESSFUL");
                    }
                    else if (message.StartsWith("IM;") && message.Length > "IM;".Length)
                    {
                        /// Present yourself:
                        /// 
                        /// Request:
                        /// IM;PLAYER_NAME
                        /// 
                        /// Response:
                        /// IM ILLFORMED
                        /// or
                        /// IM SUCCESSFUL
                        /// 

                        string name = message.Substring("IM;".Length);
                        if (name.Contains(';'))
                        {
                            LogError("Encountered bad IM request: " + message);
                            Send("IM ILLFORMED");
                            continue;
                        }

                        clientName = name;
                        Send("IM SUCCESSFUL");
                    }
                    else if (message.StartsWith("HASH;") && message.Length > "HASH;".Length)
                    {
                        /// Present your hash of files:
                        /// 
                        /// Request:
                        /// HASH;HASH
                        /// 
                        /// Response:
                        /// HASH ILLFORMED
                        /// or
                        /// HASH SUCCESSFUL
                        /// or
                        /// HASH WRONG
                        /// 

                        string hash = message.Substring("HASH;".Length);
                        if (hash.Contains(';'))
                        {
                            LogError("Encountered bad HASH request: " + message);
                            Send("HASH ILLFORMED");
                            continue;
                        }

                        clientHash = hash;
                        if (clientHash == serverHash)
                        {
                            Send("HASH SUCCESSFUL");
                        }
                        else
                        {
                            Send("HASH WRONG");
                        }
                    }
                    else if (message == "DC")
                    {
                        /// Tell that you're disconnecting:
                        /// 
                        /// Request:
                        /// DC
                        /// 
                        /// Response:
                        /// DC SUCCESSFUL
                        /// 

                        EndGame();
                        LogInfo("Player " + (clientName != null ? clientName + " " : "") + "disconnected.");
                        Send("DC SUCCESSFUL");

                        return false;
                    }
                }
                catch (SocketException ex)
                {
                    // when for example client disconnects without notice
                    LogError(ex.ToString());
                    IPPort ipPort = GetIPv4AndPort(clientSocket);
                    LogError("Connection to " + ipPort.ip + ":" + ipPort.port + " is being closed...");
                    return false;
                }
            }
        }

        private void EndGame()
        {
            if (clientGame != null)
            {
                LogInfo("Ending game " + clientGame.Name);
                games.Remove(clientGame);
                // TODO tell other players about it
            }
        }

        private bool HandleInsideGame()
        {
            return true; // TODO
        }

        private bool IsReadyForGame()
        {
            if (clientName == "")
            {
                LogError("Client is missing a name to play");
                return false;
            }
            if (clientHash != serverHash)
            {
                LogError("Client has a different hash of files - expected: " + serverHash + " got: " + clientHash);
                return false;
            }
            return true;
        }

        private void Send(string text)
        {
            clientSocket.Send(Encoding.ASCII.GetBytes(text));
        }

        private string Receive()
        {
            byte[] buffer = new byte[1024];
            int receivedBytes = clientSocket.Receive(buffer);
            return Encoding.ASCII.GetString(buffer, 0, receivedBytes);
        }

        static private readonly int interval = 500;

        private readonly Socket clientSocket;
        private readonly Action<Game, Socket> playerReadyNotifier;
        private readonly List<Game> games;
        private readonly string serverHash;
        private string clientName = "";
        private string clientHash;
        private ClientStatus clientStatus = ClientStatus.Undefined;
        private Game clientGame;
    }
}
