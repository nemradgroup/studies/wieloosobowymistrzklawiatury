﻿using System.Collections.Generic;
using System.Net.Sockets;
using System.Text;
using WieloosobowyMistrzKlawiaturyLib.Game;
using static WieloosobowyMistrzKlawiaturyLib.Utils.Logging;

namespace WieloosobowyMistrzKlawiaturyServer
{
    /// <summary>
    /// Handler that joins a few connections into one, responsible for the game
    /// </summary>
    class LinkedConnectionHandler
    {
        public LinkedConnectionHandler(Game game)
        {
            Game = game;
        }

        public void RegisterSocket(Socket clientSocket)
        {
            clientSockets.Add(clientSocket);
            if (!Game.CanPlayerJoin() && Game.Players.Length == clientSockets.Count)
            {
                LogInfo("Game: " + Game.Name + " is starting...");
                SendToAll("GAME STARTING");
                // TODO send signal about start to everyone
            }
        }

        private void SendToAll(string message)
        {
            foreach (Socket socket in clientSockets)
            {
                socket.Send(Encoding.ASCII.GetBytes(message));
            }
        }

        private void SendToAllExceptOne(string message, Socket notReceiver)
        {
            foreach (Socket socket in clientSockets)
            {
                if (socket != notReceiver)
                {
                    socket.Send(Encoding.ASCII.GetBytes(message));
                }
            }
        }

        public Game Game { get; }
        private List<Socket> clientSockets = new List<Socket>();
    }
}
