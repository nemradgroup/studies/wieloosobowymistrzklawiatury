﻿using System.Collections.Generic;
using System.Net;
using System.Net.Sockets;
using System.Threading;
using WieloosobowyMistrzKlawiaturyLib.Game;
using WieloosobowyMistrzKlawiaturyLib.Utils;
using static WieloosobowyMistrzKlawiaturyLib.Utils.NetworkUtils;
using static WieloosobowyMistrzKlawiaturyLib.Utils.Logging;

namespace WieloosobowyMistrzKlawiaturyServer
{
    /// <summary>
    /// Main class on the server side
    /// </summary>
    class Program
    {
        static void Main(string[] args)
        {
            Program program = new Program(44000, 10);
            program.StartListeningForConnections();
        }

        public Program(int listeningPort, int maxClients)
        {
            this.listeningPort = listeningPort;
            this.maxClients = maxClients;

            hashOfFiles = new FilesUtils("data/texts").GetHashOfDataDir();

            ThreadPool.SetMaxThreads(this.maxClients + 1, this.maxClients + 1);
        }

        public void PrepareLinkedConnectionHandler(Game game, Socket clientSocket)
        {
            bool found = false;
            foreach (LinkedConnectionHandler handler in linkedConnectionHandlers)
            {
                if (ReferenceEquals(handler.Game, game))
                {
                    handler.RegisterSocket(clientSocket);
                    found = true;
                    break;
                }
            }
            if (!found)
            {
                LinkedConnectionHandler handler = new LinkedConnectionHandler(game);
                handler.RegisterSocket(clientSocket);
                linkedConnectionHandlers.Add(handler);
            }
        }

        public void StartListeningForConnections()
        {
            IPHostEntry ipHostInfo = Dns.GetHostEntry(Dns.GetHostName());
            IPAddress ipAddress = ipHostInfo.AddressList[0];
            IPEndPoint localEndPoint = new IPEndPoint(ipAddress, listeningPort);

            Socket listener = new Socket(ipAddress.AddressFamily, SocketType.Stream, ProtocolType.Tcp);
            try
            {
                listener.Bind(localEndPoint);
                listener.Listen(maxClients);

                listeningThread = new Thread(() => StartListeningForConnections(listener));
                listeningThread.Start();
                LogInfo("Listening on port: " + listeningPort.ToString());
            }
            catch (SocketException ex)
            {
                LogError("Listening failed: " + ex.Message);
            }
        }

        private void StartListeningForConnections(Socket listener)
        {
            while (true)
            {
                Socket client = listener.Accept();

                IPPort ipPort = GetIPv4AndPort(client);
                LogInfo("Connected to a new client, IP: " + ipPort.ip + " port: " + ipPort.port.ToString());

                ConnectionHandler connectionHandler = new ConnectionHandler(CreatedGames, hashOfFiles, client, PrepareLinkedConnectionHandler);
                ThreadPool.QueueUserWorkItem(new WaitCallback((callback) => connectionHandler.HandleOutsideGame()));
            }
        }

        private readonly int listeningPort;
        private readonly int maxClients;

        private string hashOfFiles;
        private Thread listeningThread;
        public List<Game> CreatedGames { get; } = new List<Game>();
        private List<LinkedConnectionHandler> linkedConnectionHandlers = new List<LinkedConnectionHandler>();
    }
}
